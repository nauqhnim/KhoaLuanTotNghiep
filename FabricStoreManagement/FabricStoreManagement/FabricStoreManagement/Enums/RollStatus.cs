﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Enums
{
    public enum RollStatus
    {
        PreImport,
        Importing,
        Imported,
        PreExport,
        Exporting,
        Exported
    }
}
