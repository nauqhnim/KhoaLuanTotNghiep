﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Enums
{
    public enum ReceiptStatus
    {
        PreImport,
        Importing,
        Imported
    }
}
