﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using FabricStoreManagement.Enums;
using FabricStoreManagement.Models;
using OfficeOpenXml;

namespace FabricStoreManagement.ExcelFileServices
{
    public class ExcelFileService : IExcelFileService
    {
        #region ImportFile

        public Receipts ImportFile(string filePath)
        {
            var receipt = new Receipts();
            var rolls = new ObservableCollection<Rolls>();

            FileInfo existingFile = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                //get the first worksheet in the workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

                var index = 1;
                for (index = 1; index >= 0 && index < 50; index++)
                {
                    if (worksheet.Cells[index, 1].Value != null &&
                        (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("II. GENERAL INFORMATION"))
                    {
                        index++;
                        break;
                    }
                }

                for (; index >= 0 && index < 50; index++)
                {
                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("SUPPLIER") ||
                         (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("NHÀ CUNG CẤP VẢI")))
                    {
                        receipt.Supplier = (worksheet.Cells[$"B{index}"].Value != null)
                            ? worksheet.Cells[$"B{index}"].Value.ToString()
                            : null;
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("PO NO") ||
                        (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("SỐ PO")))
                    {
                        receipt.POId = (worksheet.Cells[$"B{index}"].Value != null)
                            ? worksheet.Cells[$"B{index}"].Value.ToString()
                            : null;
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("FABRIC CODE") ||
                         (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("MÃ VẢI")))
                    {
                        receipt.FabricCodeId = (worksheet.Cells[$"B{index}"].Value != null)
                            ? worksheet.Cells[$"B{index}"].Value.ToString()
                            : null;
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("TOTAL ROLLS") ||
                         (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("TỔNG SỐ ROLLS")))
                    {
                        receipt.NumberOfRolls = Int16.Parse(worksheet.Cells[$"B{index}"].Value.ToString());
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("TOTAL YARDS") ||
                         (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("TỔNG SỐ YARDS")))
                    {
                        receipt.TotalYards = Int16.Parse(worksheet.Cells[$"B{index}"].Value.ToString());
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        ((worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("NOTE") ||
                         (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("GHI CHÚ")))
                    {
                        receipt.Note = (worksheet.Cells[$"B{index}"].Value != null)
                            ? worksheet.Cells[$"B{index}"].Value.ToString()
                            : null;
                    }

                    if (worksheet.Cells[index, 1].Value != null &&
                        (worksheet.Cells[index, 1].Value.ToString().ToUpper()).Contains("III. ROLL INFORMATION"))
                    {
                        index++;
                        index++; // continur to add
                        break;
                    }
                }

                //Import file
                for (; index >= 0 && worksheet.Cells[index, 1].Value != null; index++)
                {
                    var roll = new Rolls
                    {
                        RollId = $"{receipt.POId} {DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss")}",
                        Status = RollStatus.PreImport,
                        Yard = double.Parse((worksheet.Cells[$"C{index}"].Value).ToString()),
                        Shade = worksheet.Cells[$"E{index}"].Value.ToString(),
                    };
                    rolls.Add(roll);
                }

            } // the using statement automatically calls Dispose() which closes the package.

            receipt.Rolls = new ObservableCollection<Rolls>(rolls);

            return receipt;

        }

        #endregion

        #region ExportFile

        

        #endregion
    }
}
