﻿using System.Collections.ObjectModel;
using FabricStoreManagement.Models;

namespace FabricStoreManagement.ExcelFileServices
{
    public interface IExcelFileService
    {
        Receipts ImportFile(string filePath);
    }
}
