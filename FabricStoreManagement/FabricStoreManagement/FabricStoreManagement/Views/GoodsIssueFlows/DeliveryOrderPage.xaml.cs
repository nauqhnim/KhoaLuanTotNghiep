﻿using FabricStoreManagement.Views.Base;
using Xamarin.Forms.Xaml;

namespace FabricStoreManagement.Views.GoodsIssueFlows
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeliveryOrderPage : BasePage
	{
		public DeliveryOrderPage ()
		{
			InitializeComponent ();
		}
	}
}