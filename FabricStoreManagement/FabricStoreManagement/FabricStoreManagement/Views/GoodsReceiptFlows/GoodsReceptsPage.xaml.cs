﻿using FabricStoreManagement.Views.Base;
using Xamarin.Forms.Xaml;

namespace FabricStoreManagement.Views.GoodsReceiptFlows
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GoodsReceptsPage : BasePage
	{
		public GoodsReceptsPage ()
		{
			InitializeComponent ();
		}
	}
}