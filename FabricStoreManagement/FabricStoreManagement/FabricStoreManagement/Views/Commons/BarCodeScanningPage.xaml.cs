﻿using FabricStoreManagement.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;
using ZXing.Mobile;

namespace FabricStoreManagement.Views.Commons
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BarCodeScanningPage : BasePage
	{
		public BarCodeScanningPage ()
		{
			InitializeComponent ();

            Scanner.Options = new MobileBarcodeScanningOptions()
            {
                DelayBetweenContinuousScans = 2000,
                DelayBetweenAnalyzingFrames = 2000,
                DisableAutofocus = false,
                TryHarder = true,
                UseNativeScanning = true,
                PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.CODE_128 }
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Scanner.IsAnalyzing = true;
            Scanner.IsScanning = true;
        }

        protected override void OnDisappearing()
        {
            Scanner.IsAnalyzing = false;
            Scanner.IsScanning = false;
            base.OnDisappearing();
        }
    }
}