﻿using System;
using System.Threading.Tasks;
using FabricStoreManagement.Constants;
using FabricStoreManagement.ExcelFileServices;
using FabricStoreManagement.Interfaces.LocalDatabase;
using FabricStoreManagement.Managers;
using FabricStoreManagement.Models;
using FabricStoreManagement.Services.HttpService;
using FabricStoreManagement.Services.SQLiteService;
using Prism;
using Prism.Ioc;
using FabricStoreManagement.ViewModels;
using FabricStoreManagement.ViewModels.Commons;
using FabricStoreManagement.ViewModels.GoodsIssueFlows;
using FabricStoreManagement.ViewModels.GoodsReceiptFlows;
using FabricStoreManagement.Views;
using FabricStoreManagement.Views.Commons;
using FabricStoreManagement.Views.GoodsIssueFlows;
using FabricStoreManagement.Views.GoodsReceiptFlows;
using FotoScan.Tablet.Interfaces.HttpService;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FabricStoreManagement
{
    public partial class App : PrismApplication
    {
        #region Constructor

        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        #endregion

        #region Properties 

        public new static App Current => Application.Current as App;
        public static double ScreenWidth;
        public static double ScreenHeight;

        public static bool IsBusy = true;

        private ISqLiteService _sqLiteService;
        public static AppSettings Settings { get; set; }

        #endregion

        #region OnInitialized

        protected override async void OnInitialized()
        {
            InitAppCenter();
            InitDatabase();
            InitializeComponent();

            await StartApp();
        }

        #endregion

        #region InitAppCenter

        private void InitAppCenter()
        {
            AppCenter.Start($"android={AppCenterConstant.AppCenterAndroid};" +
                            $"ios={AppCenterConstant.AppCenteriOS}",
                typeof(Analytics), typeof(Crashes));
        }

        #endregion

        #region InitDatabase

        private void InitDatabase()
        {
            var connectionService = DependencyService.Get<IDatabaseConnection>();
            _sqLiteService = new SqLiteService(connectionService);
        }

        #endregion

        #region StartApp

        private async Task StartApp()
        {
            Settings = new AppSettings();
            Settings = _sqLiteService.GetSettings();

            string uri = PageManager.TestPage;
            await NavigationService.NavigateAsync(new Uri($"https://quanvm.com/{uri}"));
            //await NavigationService.NavigateAsync($"{PageManager.MainPage}");
        }

        #endregion

        #region RegisterTypes

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<DeliveryOrderPage, DeliveryOrderPageViewModel>();
            containerRegistry.RegisterForNavigation<GoodsReceptsPage, GoodsReceptsPageViewModel>();
            containerRegistry.RegisterForNavigation<SettingsPage, SettingsPageViewModel>();
            containerRegistry.RegisterForNavigation<BarCodeScanningPage, BarCodeScanningPageViewModel>(); 

            containerRegistry.RegisterForNavigation<TestPage, TestPageViewModel>();

            // Init Service
            containerRegistry.Register<IHttpRequest, HttpRequest>();
            containerRegistry.Register<ISqLiteService, SqLiteService>();
            containerRegistry.Register<IExcelFileService, ExcelFileService>();
        }

        #endregion
    }
}
