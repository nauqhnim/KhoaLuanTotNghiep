﻿using FabricStoreManagement.Interfaces;
using Xamarin.Forms;

namespace FabricStoreManagement.Constants
{
    public class FileConstants
    {
        private static readonly string FilePath = DependencyService.Get<IFileService>().FilePath;

        public static string AppPath()
        {
            return $"{FilePath}/Fabric Store/";
        }
    }
}
