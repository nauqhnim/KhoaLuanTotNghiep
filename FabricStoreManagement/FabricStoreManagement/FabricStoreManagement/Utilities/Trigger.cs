﻿using System.Globalization;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace FabricStoreManagement.Utilities
{
    public class FormatNumberValidationTriggerAction : TriggerAction<Entry>
    {
        protected override void Invoke(Entry sender)
        {
            if (string.IsNullOrEmpty(sender.Text)) return;
            var resultTextValue = Regex.Replace(sender.Text, @"[^0-9]+", "");

            if (string.IsNullOrEmpty(resultTextValue) || resultTextValue == "0")
            {
                sender.Text = "";
                return;
            }

            sender.Text = long.Parse(resultTextValue).ToString("N0", new CultureInfo("en-US"));
        }
    }
}
