﻿using FabricStoreManagement.Enums;
using System;
using System.Collections.ObjectModel;
using SQLite;

namespace FabricStoreManagement.Models
{
    public class Receipts
    {
        [PrimaryKey]
        public string Id { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss");

        public string POId { get; set; }

        public string FabricCodeId { get; set; }

        public string Supplier { get; set; }

        public string ImportUser { get; set; }

        public string Barcode { get; set; }

        public ReceiptStatus Status { get; set; }

        public int NumberOfRolls { get; set; }

        public int TotalYards { get; set; }

        public string Note { get; set; }

        [Ignore]
        public ObservableCollection<Rolls> Rolls { get; set; }
    
    }
}
