﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Duty { get; set; }
        public string PhoneNumber { get; set; }
        public string IdNumber { get; set; }
        public DateTime DayOfBirth { get; set; }
    }
}
