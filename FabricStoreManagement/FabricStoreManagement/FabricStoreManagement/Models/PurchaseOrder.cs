﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class PurchaseOrder
    {
        public string Id { get; set; }
        public string FabricId { get; set; }
        public string Provider { get; set; }
    }
}
