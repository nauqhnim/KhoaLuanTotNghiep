﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class TestModel
    {
        public string Brand { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
    }
}
