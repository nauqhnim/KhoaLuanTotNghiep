﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class ReceiptDetails
    {
        public string Id { get; set; }
        public string ReceiptId { get; set; }
        public Rolls Rolls { get; set; }
    }
}
