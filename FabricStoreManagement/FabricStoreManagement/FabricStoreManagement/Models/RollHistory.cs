﻿using FabricStoreManagement.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class RollHistory
    {
        public string Id { get; set; }
        public string RollId { get; set; }

        public string RollNumber { get; set; }

        public int Yard { get; set; }

        public DateTime UpdatedTime { get; set; }

        public RollStatus Status { get; set; }

        public string Position { get; set; }

        public string UpdatingUser { get; set; }

        public RollHistory()
        {
        }
    }
}
