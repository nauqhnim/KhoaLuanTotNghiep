﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class SaleOrder
    {
        public string Id { get; set; }
        public string Orderer { get; set; }
    }
}
