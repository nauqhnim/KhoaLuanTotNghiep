﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class OrderDetails
    {
        public string Id { get; set; }
        public string POId { get; set; }
        public string SOId { get; set; }
    }
}
