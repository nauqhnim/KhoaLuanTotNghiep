﻿using System;
using System.Collections.Generic;
using System.Text;
using FabricStoreManagement.Enums;

namespace FabricStoreManagement.Models
{
    public class Rolls
    {
        public string RollId {get;set;}
        public string RollNumber { get; set; }
        public string BarCode { get; set; }
        public double Yard { get; set; }
        public string Shade { get; set; }

        public RollStatus Status { get; set; }
        public string OrderDetailsId { get; set; }

        public Rolls()
        {
        }

        public Rolls(string rollid, string rollnumber, string barcode)
        {
            this.RollId = rollid;
            this.RollNumber = rollnumber;
            this.BarCode = barcode;
        }
    }
}
