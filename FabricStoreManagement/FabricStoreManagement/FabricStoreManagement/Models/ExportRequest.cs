﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class ExportRequest
    {
        public string Id { get; set; }
        public string SOId { get; set; }
        public int Yard { get; set; }
    }
}
