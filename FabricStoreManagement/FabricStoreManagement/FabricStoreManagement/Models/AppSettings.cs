﻿using SQLite;
using Xamarin.Forms;

namespace FabricStoreManagement.Models
{
    public class AppSettings
    {
        [PrimaryKey]
        public int Id { get; set; } = 0;

        public bool IsLogin { get; set; }

        public string HttpUrl { get; set; } = "http://test-fastupload.fairflexx.net/";

        public int ClientId { get; set; }

        public byte[] LogoUser { get; set; }

        public string LogoPath { get; set; }

        public string DefaultLogo { get; set; } = "splash_logo.png";


        [Ignore]
        public Point[] SketchPoints { get; set; }

        public string StringSketchPoints { get; set; }

    }
}
