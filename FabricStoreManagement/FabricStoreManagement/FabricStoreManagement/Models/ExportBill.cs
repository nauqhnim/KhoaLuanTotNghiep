﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace FabricStoreManagement.Models
{
    public class ExportBill
    {
        public string Id { get; set; }
        public string ExportRequest { get; set; }
        public string SOId { get; set; }
        public string ExportUser { get; set; }
        public DateTime ExportDate { get; set; }
        public int Yard { get; set; }
        public ObservableCollection<Rolls> Rolls { get; set; }
    }
}
