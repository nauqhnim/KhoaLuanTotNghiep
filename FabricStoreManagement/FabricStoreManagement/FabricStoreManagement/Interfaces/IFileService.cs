﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace FabricStoreManagement.Interfaces
{
    public interface IFileService
    {
        string FilePath { get; }

        bool DeleteFile(string filePath);

        string GetCsvFilePath();
    }
}