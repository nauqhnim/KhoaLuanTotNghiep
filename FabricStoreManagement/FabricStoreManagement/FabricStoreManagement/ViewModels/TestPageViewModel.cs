﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FabricStoreManagement.Enums;
using FabricStoreManagement.ExcelFileServices;
using FabricStoreManagement.Models;
using FabricStoreManagement.ViewModels.Base;
using Javax.Net.Ssl;
using OfficeOpenXml;
using Plugin.FilePicker;
using Prism.Commands;
using Prism.Navigation;

namespace FabricStoreManagement.ViewModels
{
    public class TestPageViewModel : ViewModelBase
    {
        public TestPageViewModel(INavigationService navigationService, IExcelFileService excelFileService)
            : base(navigationService, excelFileService: excelFileService)
        {
            Title = "Main Page";
            PickAFileCommand = new DelegateCommand(async () => { await PickAFileExecute(); });

        }



        public ICommand PickAFileCommand { get; }

        private async Task PickAFileExecute()
        {
            var file = await CrossFilePicker.Current.PickFile(allowedTypes: new[]
                {"application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});

            if (file == null)
                return; // user canceled file picking

            var a = ExcelFileService.ImportFile(filePath: file.FilePath);


            var a1 = ExcelFileService.ImportFile(filePath: file.FilePath);

            /*FileInfo existingFile = new FileInfo(file.FilePath);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                //get the first worksheet in the workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

                var index = 1;
                var address = string.Empty;
                var indexPO = string.Empty;
                for (index =1; index >= 0; index++)
                {
                    var cell = worksheet.Cells[Row: index, Col: 1];
                    var value = cell.Value;
                    if (value != null && ((string)value).Contains("PO NO."))
                    {
                        indexPO = worksheet.Cells[$"B{index}"].Address;
                    }
                    if (value != null && (string)value == "NO.")
                    {
                        address = worksheet.Cells[$"A{index}"].Address;
                        break;
                    }
                }

                //Import file
                index++;
                ObservableCollection<Rolls> rolls = new ObservableCollection<Rolls>();
                for (; index >= 0 && worksheet.Cells[index, 1].Value != null; index++)
                {
                    var roll = new Rolls
                    {
                        RollId = $"{indexPO} {DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss")}",
                        Status = RollStatus.PreImport,
                        Yard = double.Parse((worksheet.Cells[$"C{index}"].Value).ToString()),
                        //Yard = (double)worksheet.Cells[$"C{index}"].Value,
                        Shade = (string)worksheet.Cells[$"E{index}"].Value,
                    };
                    rolls.Add(roll);
                }

            } // the using statement automatically calls Dispose() which closes the package.
            */

        }
    }
}
