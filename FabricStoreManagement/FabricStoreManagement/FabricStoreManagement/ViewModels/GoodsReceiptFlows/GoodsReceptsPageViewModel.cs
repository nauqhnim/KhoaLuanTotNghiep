﻿using FabricStoreManagement.ViewModels.Base;
using Prism.Navigation;

namespace FabricStoreManagement.ViewModels.GoodsReceiptFlows
{
    public class GoodsReceptsPageViewModel : ViewModelBase
    {
        public GoodsReceptsPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
        }
    }
}
