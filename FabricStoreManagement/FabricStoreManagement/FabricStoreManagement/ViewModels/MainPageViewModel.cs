﻿using Prism.Navigation;
using FabricStoreManagement.ViewModels.Base;

namespace FabricStoreManagement.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
        }
    }
}
