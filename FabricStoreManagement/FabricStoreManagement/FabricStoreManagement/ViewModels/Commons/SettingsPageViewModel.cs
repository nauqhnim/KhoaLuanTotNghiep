﻿using System;
using System.IO;
using System.Text;
using FabricStoreManagement.ViewModels.Base;
using Plugin.FilePicker;
using Prism.Commands;
using Prism.Navigation;
using System.Threading.Tasks;
using System.Windows.Input;
using OfficeOpenXml;

namespace FabricStoreManagement.ViewModels.Commons
{
    public class SettingsPageViewModel : ViewModelBase
    {
        public SettingsPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
            PickAFileCommand = new DelegateCommand(async() => { await PickAFileExecute(); });
        }

        public ICommand PickAFileCommand { get; }

        private async Task PickAFileExecute()
        {
            var file = await CrossFilePicker.Current.PickFile();
            var a = File.ReadAllText(file.FilePath, Encoding.UTF8);

            var filepath = file.FilePath.Replace(".xlsx", "1.csv");
            var localPublicFilePath = Path.Combine(filepath);
            File.WriteAllText(localPublicFilePath, a, Encoding.UTF8);

            var b = File.ReadAllText(filepath);


            var c = File.ReadAllText(filepath);

            //Spreadsheet document = new Spreadsheet())

            //document.LoadFromFile("SimpleReport.xlsx");
            //string csvFile = Path.GetTempPath() + "SimpleReport.csv";

            //// Save the document as CSV file
            //document.Workbook.Worksheets[0].SaveAsCSV(csvFile);
            //document.Close();


            FileInfo existingFile = new FileInfo(file.FilePath);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                //get the first worksheet in the workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                int col = 2; //The item description
                //output the data in column 2
                for (int row = 2; row < 5; row++)
                    Console.WriteLine("\tCell({0},{1}).Value={2}", row, col, worksheet.Cells[row, col].Value);

                //output the formula in row 5
                Console.WriteLine("\tCell({0},{1}).Formula={2}", 3, 5, worksheet.Cells[3, 5].Formula);
                Console.WriteLine("\tCell({0},{1}).FormulaR1C1={2}", 3, 5, worksheet.Cells[3, 5].FormulaR1C1);

                //output the formula in row 5
                Console.WriteLine("\tCell({0},{1}).Formula={2}", 5, 3, worksheet.Cells[5, 3].Formula);
                Console.WriteLine("\tCell({0},{1}).FormulaR1C1={2}", 5, 3, worksheet.Cells[5, 3].FormulaR1C1);

            } // the using statement automatically calls Dispose() which closes the package.

            //var a = File.ReadAllText(file.FilePath);
            //var list = new List<TestModel>();
            //FileStream stream = File.Open(file.FilePath, FileMode.Open, FileAccess.Read);

            ////1. Reading from a binary Excel file ('97-2003 format; *.xls)
            ////IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            ////...
            ////2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);


            ////5. Data Reader methods
            //while (excelReader.Read())
            //{
            //    var obj = new TestModel
            //    {
            //        Brand = excelReader.GetDataTypeName(0),
            //        ProductCode = excelReader.GetDataTypeName(1),
            //        Description = excelReader.GetString(2),
            //    };

            //    list.Add(obj);
            //}

            //6. Free resources (IExcelDataReader is IDisposable)
            //excelReader.Close();
            //var json = new JavaScriptSerializer().Serialize(list);

        }
    }
}
