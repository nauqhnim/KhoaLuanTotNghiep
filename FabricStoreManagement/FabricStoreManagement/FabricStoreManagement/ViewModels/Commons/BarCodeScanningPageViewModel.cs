﻿using FabricStoreManagement.Utilities;
using FabricStoreManagement.ViewModels.Base;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZXing;

namespace FabricStoreManagement.ViewModels.Commons
{
    public class BarCodeScanningPageViewModel : ViewModelBase
    {
        public BarCodeScanningPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            ResultCommand = new DelegateCommand<Result>(async result => await ScanResult(result));
        }

        public bool IsAnalyzing { get; set; } = true;
        public bool IsScanning { get; set; } = true;
        public string ScanType { get; set; }

        public ICommand ResultCommand { get; set; }

        #region OnNavigateTo
        
        public override void OnNavigatedNewTo(INavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey("ScanType"))
            {
                ScanType = (string)parameters["ScanType"];
            }
        }

        #endregion

        private async Task ScanResult(Result result)
        {
            var navigationParams = new NavigationParameters();

            if (ScanType != null)
            {
                navigationParams.Add("ScanResult", result);
                navigationParams.Add("ScanType", ScanType);
            }
            else
            {
                navigationParams.Add("ScanResult", result);
            }

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                await Navigation.GoBackAsync(navigationParams);
            });
        }
    }
}
