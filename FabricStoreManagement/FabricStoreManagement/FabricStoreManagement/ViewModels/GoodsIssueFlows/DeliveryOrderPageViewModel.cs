﻿using FabricStoreManagement.ViewModels.Base;
using Prism.Navigation;

namespace FabricStoreManagement.ViewModels.GoodsIssueFlows
{
    public class DeliveryOrderPageViewModel : ViewModelBase
    {
        public DeliveryOrderPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
        }
    }
}
