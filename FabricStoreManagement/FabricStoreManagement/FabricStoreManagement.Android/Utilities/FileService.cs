﻿using System;
using System.IO;
using FabricStoreManagement.Constants;
using FabricStoreManagement.Droid.Utilities;
using FabricStoreManagement.Interfaces;
using Xamarin.Forms;
using Environment = Android.OS.Environment;

[assembly: Dependency(typeof(FileService))]
namespace FabricStoreManagement.Droid.Utilities
{
    public class FileService : IFileService
    {
        #region FilePath & Properties

        public string FilePath { get; } = Environment.ExternalStorageDirectory.ToString();

        #endregion

        #region DeleteFile

        public bool DeleteFile(string filePath)
        {
            if (!File.Exists(filePath)) return false;

            File.Delete(filePath);
            return true;
        }

        #endregion

        public bool InitStorageUpload(string filePath)
        {
            if (File.Exists(filePath)) return false;

            Directory.CreateDirectory(filePath);
            return true;
        }

        #region GetCsvFilePath

        #region CreateFilePath

        private string CreatekAppFilePath()
        {
            return FileConstants.AppPath();
        }

        #endregion

        public string GetCsvFilePath()
        {
            return CreatekAppFilePath()+ "ExcelFile.xlsx";
        }

        #region CheckFileNameExists

        // Check if the fileService has the same name or not 
        public bool CheckFileNameExists(string filePath)
        {
            return File.Exists(filePath);
        }

        #endregion

        #endregion
    }
}